package br.com.gleysongama.bank.model;

public enum TipoConta {
    CORRENTE(0),
    POUPANCA(1);

    private int tipo;

    TipoConta(int tipo) {
        this.tipo = tipo;
    }

    public int getTipo() {
        return this.tipo;
    }
}