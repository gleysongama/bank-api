package br.com.gleysongama.bank.model;

public enum TipoSabor {
    CALABRESA("Calabresa"),
    MARGUERITA("Marguerita"),
    PORTUGUESA("Portuguesa");

    private String tipo;

    TipoSabor(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return this.tipo;
    }
}