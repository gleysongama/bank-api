package br.com.gleysongama.bank.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Contas {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idConta;

    private BigDecimal saldo;
    
    private BigDecimal limiteSaqueDiario;
    
    @Column(nullable = false)
    private boolean flagAtivo;
    
    @Column(nullable = false)
    private TipoConta tipoConta;
    
	@CreationTimestamp
	@Column(nullable = false)
	private LocalDateTime dataCriacao;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idPessoa", nullable = false)
    private Pessoas pessoa;
    
    @OneToMany(mappedBy = "conta", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Transacoes> transacoes = new ArrayList<Transacoes>();
    
	public Contas() {
		super();
	}

	public Contas(BigDecimal saldo, BigDecimal limiteSaqueDiario, boolean flagAtivo, TipoConta tipoConta, Pessoas pessoa, List<Transacoes> transacoes) {
		super();
		this.saldo = Objects.isNull(saldo) ? BigDecimal.ZERO : saldo;
		this.limiteSaqueDiario = limiteSaqueDiario;
		this.flagAtivo = flagAtivo;
		this.tipoConta = tipoConta;
		this.pessoa = pessoa;
		this.transacoes = transacoes;
	}



	public Long getIdConta() {
		return idConta;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public BigDecimal getLimiteSaqueDiario() {
		return limiteSaqueDiario;
	}

	public void setLimiteSaqueDiario(BigDecimal limiteSaqueDiario) {
		this.limiteSaqueDiario = limiteSaqueDiario;
	}

	public boolean isFlagAtivo() {
		return flagAtivo;
	}

	public void setFlagAtivo(boolean flagAtivo) {
		this.flagAtivo = flagAtivo;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Pessoas getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoas pessoa) {
		this.pessoa = pessoa;
	}

	public List<Transacoes> getTransacoes() {
		return transacoes;
	}

	public void setTransacoes(List<Transacoes> transacoes) {
		this.transacoes = transacoes;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataCriacao == null) ? 0 : dataCriacao.hashCode());
		result = prime * result + (flagAtivo ? 1231 : 1237);
		result = prime * result + ((idConta == null) ? 0 : idConta.hashCode());
		result = prime * result + ((limiteSaqueDiario == null) ? 0 : limiteSaqueDiario.hashCode());
		result = prime * result + ((pessoa == null) ? 0 : pessoa.hashCode());
		result = prime * result + ((saldo == null) ? 0 : saldo.hashCode());
		result = prime * result + ((tipoConta == null) ? 0 : tipoConta.hashCode());
		result = prime * result + ((transacoes == null) ? 0 : transacoes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contas other = (Contas) obj;
		if (dataCriacao == null) {
			if (other.dataCriacao != null)
				return false;
		} else if (!dataCriacao.equals(other.dataCriacao))
			return false;
		if (flagAtivo != other.flagAtivo)
			return false;
		if (idConta == null) {
			if (other.idConta != null)
				return false;
		} else if (!idConta.equals(other.idConta))
			return false;
		if (limiteSaqueDiario == null) {
			if (other.limiteSaqueDiario != null)
				return false;
		} else if (!limiteSaqueDiario.equals(other.limiteSaqueDiario))
			return false;
		if (pessoa == null) {
			if (other.pessoa != null)
				return false;
		} else if (!pessoa.equals(other.pessoa))
			return false;
		if (saldo == null) {
			if (other.saldo != null)
				return false;
		} else if (!saldo.equals(other.saldo))
			return false;
		if (tipoConta != other.tipoConta)
			return false;
		if (transacoes == null) {
			if (other.transacoes != null)
				return false;
		} else if (!transacoes.equals(other.transacoes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Contas [idConta=" + idConta + ", saldo=" + saldo + ", limiteSaqueDiario=" + limiteSaqueDiario
				+ ", flagAtivo=" + flagAtivo + ", tipoConta=" + tipoConta + ", pessoa=" + pessoa + ", transacoes="
				+ transacoes + ", dataCriacao=" + dataCriacao + "]";
	}
}
