package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Transacoes;

public interface TransacoesRepository extends JpaRepository<Transacoes, Long>{

}
