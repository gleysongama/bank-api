package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Pessoas;

public interface PessoasRepository extends JpaRepository<Pessoas, Long>{

}
