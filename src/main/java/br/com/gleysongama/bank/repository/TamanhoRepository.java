package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Tamanho;
import br.com.gleysongama.bank.model.TipoTamanho;

public interface TamanhoRepository extends JpaRepository<Tamanho, Long> {
    public Tamanho findByTipoTamanho(TipoTamanho tipoTamanho);
}