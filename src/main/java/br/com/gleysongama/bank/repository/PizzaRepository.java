package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Pizza;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {
    
}