package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {
    
}