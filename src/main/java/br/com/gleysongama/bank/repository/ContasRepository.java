package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Contas;

public interface ContasRepository extends JpaRepository<Contas, Long>{

}
