package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Sabor;
import br.com.gleysongama.bank.model.TipoSabor;

public interface SaborRepository extends JpaRepository<Sabor, Long> {
    public Sabor findByTipoSabor(TipoSabor tipoSabor);
}