package br.com.gleysongama.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gleysongama.bank.model.Opcional;
import br.com.gleysongama.bank.model.TipoOpcional;

public interface OpcionalRepository extends JpaRepository<Opcional, Long> {
    public Opcional findByTipoOpcional(TipoOpcional tipoOpcional);
}