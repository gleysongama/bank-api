package br.com.gleysongama.bank.api.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import br.com.gleysongama.bank.model.Pessoas;
import br.com.gleysongama.bank.model.TipoConta;

public class ContasRequest {	
	private BigDecimal saldo;
	
	@NotNull(message = "Informe o limite de saque diário para a conta")    
    private BigDecimal limiteSaqueDiario;
    
	@NotNull(message = "Informe se a conta está ativa")
    private boolean flagAtivo;
    
	@NotNull(message = "Informe o tipo de conta")
    private TipoConta tipoConta;
    
	@NotNull(message = "Não é permitido criar conta sem informar uma pessoa")
    private Pessoas pessoa;
    
	public ContasRequest() {
		super();
	}

	public ContasRequest(BigDecimal saldo, BigDecimal limiteSaqueDiario, boolean flagAtivo, TipoConta tipoConta, Pessoas pessoa) {
		super();
		this.saldo = saldo;
		this.limiteSaqueDiario = limiteSaqueDiario;
		this.flagAtivo = flagAtivo;
		this.tipoConta = tipoConta;
		this.pessoa = pessoa;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public BigDecimal getLimiteSaqueDiario() {
		return limiteSaqueDiario;
	}

	public void setLimiteSaqueDiario(BigDecimal limiteSaqueDiario) {
		this.limiteSaqueDiario = limiteSaqueDiario;
	}

	public boolean isFlagAtivo() {
		return flagAtivo;
	}

	public void setFlagAtivo(boolean flagAtivo) {
		this.flagAtivo = flagAtivo;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public Pessoas getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoas pessoa) {
		this.pessoa = pessoa;
	}	
}
