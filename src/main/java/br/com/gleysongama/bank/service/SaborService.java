package br.com.gleysongama.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.bank.model.Sabor;
import br.com.gleysongama.bank.repository.SaborRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SaborService {
    @Autowired
    private SaborRepository saborRepository;

    public List<Sabor> findAll() {
        return saborRepository.findAll();
    }

    public Sabor findById(Long id) {
        return saborRepository.findById(id).get();
    }

    public Sabor save(Sabor Sabor) {
        return saborRepository.save(Sabor);
    }

    public void deleteById(Long id) {
        saborRepository.deleteById(id);
    }
}