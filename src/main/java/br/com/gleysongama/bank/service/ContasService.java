package br.com.gleysongama.bank.service;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.bank.model.Contas;
import br.com.gleysongama.bank.repository.ContasRepository;

@Service
public class ContasService {
	@Autowired
	ContasRepository contasRepository;
	
	public Contas save(Contas conta) {
        return contasRepository.save(conta);
    }
	
	public Contas findById(Long id) {
        return contasRepository.findById(id).get();
    }
	
	/**
	 * Funcionalidades da tarefa
	 * */
	public Contas criarConta(Contas conta) {
		return save(conta);
	}
	
	public void depositoConta(Long idConta, BigDecimal valor) {
		Contas c = findById(idConta);
		c.setSaldo(c.getSaldo().add(valor));
		save(c);
	}
	
	public BigDecimal consultaSaldoConta(Long idConta) {
		Contas c = findById(idConta);
		return c.getSaldo();
	}
	
	public void saqueConta(Long idConta, BigDecimal valor) {
		Contas c = findById(idConta);
		c.setSaldo(c.getSaldo().subtract(valor));
		save(c);
	}
	
	public void consultaExtratoConta(Contas conta) {
		
	}
	
	public void consultaExtratoConta(Contas conta, LocalDate dataInicial, LocalDate dataFinal) {
		
	}
}
