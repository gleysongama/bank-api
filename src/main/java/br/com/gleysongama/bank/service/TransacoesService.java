package br.com.gleysongama.bank.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.bank.model.Contas;
import br.com.gleysongama.bank.model.Transacoes;
import br.com.gleysongama.bank.repository.TransacoesRepository;

@Service
public class TransacoesService {
	@Autowired
	TransacoesRepository transacoesRepository;

	public Transacoes save(Transacoes transacoes) {
        return transacoesRepository.save(transacoes);
    }
	
	public Transacoes findById(Long id) {
        return transacoesRepository.findById(id).get();
    }
	
	/**
	 * Funcionalidades da tarefa
	 * */
	
	public void registrarTransacao(Contas conta, BigDecimal valor) {
		Transacoes transacoes = new Transacoes(valor, conta);
		
	}
}
