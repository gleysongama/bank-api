package br.com.gleysongama.bank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gleysongama.bank.model.Pizza;
import br.com.gleysongama.bank.repository.PizzaRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class PizzaService {
    @Autowired
    private PizzaRepository pizzaRepository;

    public List<Pizza> findAll() {
        return pizzaRepository.findAll();
    }

    public Pizza findById(Long id) {
        return pizzaRepository.findById(id).get();
    }

    public Pizza save(Pizza Pizza) {
        return pizzaRepository.save(Pizza);
    }

    public void deleteById(Long id) {
        pizzaRepository.deleteById(id);
    }
}